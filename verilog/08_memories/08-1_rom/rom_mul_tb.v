// Design: rom multiplier
// Description: ROM-based 4-bit BCD multiplier
// Author: Jorge Juan <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Date: 23-01-2012

////////////////////////////////////////////////////////////////////////////////
// This file is free software: you can redistribute it and/or modify it under //
// the terms of the GNU General Public License as published by the Free       //
// Software Foundation, either version 3 of the License, or (at your option)  //
// any later version. You can access a copy of the GNU General Public License //
// at <http://www.gnu.org/licenses/>.                                         //
////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

// This test bench applies a configurable number of random input patterns to
// the circuit under test.
// Simulation macros and default values:
//   NP: número de patrones de simulación
//   SEED: semilla inicial para generación número aleatorios

`ifndef NP
	`define NP 20	// number of patterns
`endif
`ifndef SEED
	`define SEED 1	// initial seed for random generation
`endif

module test();

	reg [3:0] x, y;		// multiplier operands
	wire [7:0] z;		// multiplier output
	integer np;		// number of patterns (auxiliary signal)
	integer seed = `SEED;	// seed (auxiliary signal)

	// Instantiation of the Unit Under Test
	rommul4 uut (.x(x), .y(y), .z(z));

	// Simulation output control
	initial	begin
		/* 'np' is a counter that holds the remaining number of patterns
		 * to be applied. The initial value is taken from macro NP. */
		np = `NP;

		// Waveform generation
		$dumpfile("test.vcd");
		$dumpvars(0, test);

		// Text output
		$display("x	y	z");
		$display("---------------------");
		$monitor("%h	%h	%h%h",
			x, y, z[7:4], z[3:0]);
	end

	// Test generation process
	/* Each 20ns 'a', 'b' and 'cin' are assigned random values. Simulation
	 * ends after applying NP patterns. The pattern sequence can be 
	 * changed by defining a different value for SEED. Macros NP and SEED
	 * can be changed in the file or using compilation options. */
	always begin
		#20
		x = $random(seed);
		y = $random(seed);
		np = np - 1;
		if (np == 0)
			$finish;
	end

endmodule // test


/*
   EXERCISES

   2. Compile the test bench with:

        $ iverilog rom_mul.v rom_mul_tb.v

      and test its operation with:

        $ vvp a.out

      Take a look at the output text and waveforms (with gtkwave) and check 
      that the operation is correct. Correct the errors if any.

   3. Repeat the simulation with different values of NP and SEED and check the 
      results. Ej:

        $ iverilog -DNP=40 -DSEED=2 rom_mul.v rom_mul_tb.v
        $ vpp a.out

   4. Modify the design to generate the multiplication result encoded in natural
      binary instead of BCD. Modify the test bench accordingly.
*/
