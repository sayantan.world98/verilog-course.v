// Design:      alu
// File:        alu.v
// Description: Simple Arithmetic-Logic Unit (ALU)
// Author:      Jorge Juan-Chico <jjchico@gmail.com>
// Date:        22-11-2013 (initial version)

/*
   Lesson 5.3. Arithmetic-Logic Units

   In this lesson we will improve the adder-subractor designed in lesson 5.2
   to convert it into a full (but simple) Arithmetic-Logic Unit (ALU). ALU's 
   are in the inner core of any computer or processor. It is the calculator
   of the computer where all calculations involved in the execution of the
   processor instructions take place. */

//////////////////////////////////////////////////////////////////////////
// Arithmetic-Logic Unit (ALU)                                          //
//////////////////////////////////////////////////////////////////////////

/* The description of the ALU is similar to the adder-subtractor in the
 * previous lesson. The operation selection input is extended to represent
 * new operation as shown in the next table:
 *
 *   op[2:0]   Operation    z
 *   ----------------------------
 *       000	Addition	a + b
 *       001	Subtraction	a - b
 *       010	Increment	a + 1
 *       011	Decrement	a - 1
 *       100	AND         a & b
 *       101	OR          a | b
 *       110	XOR	        a ^ b
 *       111	NOT         ~a
 *
 * The data width is parametrized with parameter WIDTH, with a default value
 * of 8 bits. Thus, this description can produce ALU's of any data width.
 *
 * Data is two's complement encoded so we use signed variables. It does not
 * have a carry-in input but it has an overflow output 'ov' that is set to
 * '1' when the result in output 'f' is not correct due to an overflow (the
 * correct result needs more bits than available in 'f'). */

module alu #(
    parameter WIDTH = 8                 // data width
    )(
    input wire signed [WIDTH-1:0] a,    // first operand
    input wire signed [WIDTH-1:0] b,    // second operand
    input wire [2:0] op,                // operation
    output reg signed [WIDTH-1:0] f,    // output
    output reg ov                       // overflow output
    );

    always @* begin
        if (op[2] == 0)	begin :arithm   // Arithmetic operations
            reg signed [WIDTH:0] s;
            /* The rest of the bits in 'op' select the arithmetic
             * operation to do. The result is stored in 's' including
             * a posible additional bit. */
            case (op[1:0])
            2'b00:
                s = a + b;	// sum
            2'b01:
                s = a - b;	// subtract
            2'b10:
                s = a + 1;	// increment
            2'b11:
                s = a - 1;	// decrement
            endcase

            // Overflow calculation
            ov = (s[WIDTH] == s[WIDTH-1])? 0: 1;

            // Output
            f = s[WIDTH-1:0];
        end else begin      // Logic operations
            case (op[1:0])
            2'b00:
                f = a & b;	// AND
            2'b01:
                f = a | b;	// OR
            2'b10:
                f = a ^ b;	// XOR
            2'b11:
                /* '~' is the complement operator that complements
                 * all the bits of the operand. */
                f = ~a;     // NOT
            endcase
            // There's no overflow in logic operations
            ov = 0;
        end // if
    end // always

endmodule // alu

/*
   EXERCISE

   1. Compile the lesson with:

        $ iverilog alu.v

      Check that there are no syntax errors.

   (continues in alu_tb.v)
*/
