// Design:      alu
// File:        alu.v
// Description: Simple Arithmetic-Logic Unit (ALU). Test bench.
// Author:      Jorge Juan-Chico <jjchico@gmail.com>
// Date:        22-11-2013 (initial version)

/*
   Lesson 5.3. Arithmetic-Logic Units

   This file contains a test bench for module 'alu'.
 */
`timescale 1ns / 1ps

// This test bench applies a configurable number of random input patterns to
// the circuit under test.

// Simulation macros and default values:
//   NP: number of test patterns
//   SEED: initial seed for pseudo-random number generation
//   OP: type of operation (see 'alu.v')

`ifndef NP
    `define NP 20
`endif
`ifndef SEED
    `define SEED 1
`endif
`ifndef OP
    `define OP 0
`endif

module test ();
    
    reg signed [7:0] a;     // input 'a'
    reg signed [7:0] b;     // input 'b'
    reg [2:0] op = `OP;     // type of operation
    wire signed [7:0] f;    // output
    wire ov;                // overflow output
    integer np;             // number of patterns (auxiliary signal)
    integer seed = `SEED;   // seed (auxiliary signal)

    // Circuit under test
    /* Alu width is parametrized. We instantiate an 8 bit ALU. */
    alu #(.WIDTH(8)) uut(.a(a), .b(b), .op(op), .f(f), .ov(ov));

    initial begin
        /* 'np' is a counter that holds the remaining number of patterns
         * to be applied. The initial value is taken from macro NP. */
        np = `NP;
        
        // Waveform generation (optional)
        $dumpfile("test.vcd");
        $dumpvars(0, test);
        
        // Output printing
        $write("Operation: %d ", op);
        case (op)
        0:       $display("ADD");
        1:       $display("SUB");
        2:       $display("INCREMENT");
        3:       $display("DECREMENT");
        4:       $display("AND");
        5:       $display("OR");
        6:       $display("XOR");
        default: $display("NOT");
        endcase

        /* Inputs and output are displayed in decimal and in binary
         * formats so that both arithmetic and logic operations can be
         * esily checked. */
        $display("       A                B",
                 "                 F         ov");
        $monitor("%b (%d)  %b (%d)   %b (%d)  %b",
                   a, a, b, b, f, f, ov);
    end

    // Test generation process
    /* Each 20ns 'a', 'b' and 'cin' are assigned random values. Simulation
     * ends after applying NP patterns. The pattern sequence can be 
     * changed by defining a different value for SEED. Macros NP and SEED
     * can be changed in the file or using compilation options. */
    always begin
        #20
        a = $random(seed);
        b = $random(seed);
        np = np - 1;
        if (np == 0)
            $finish;
    end
endmodule

/*
   EXERCISES
    
   2. Compile the design with:
    
        $ iverilog alu.v alu_tb.v
      
      and simulate the design with:
      
        $ vvp a.out
      
      Check that the operation is correct for the addition case by looking at
      the test output and the waveforms with gtkwave.
      
      Check the rest of the operations in a similar way by defining a different
      value for macro OP (between 0 and 7). For example:
      
      $ iverilog -DOP=1 alu.v alu_tb.v
      
      NOTE: to check the correct operation of the overflow for increment and
      decrement operations you may have to increase the number of patterns to
      simulate by defining a higher value of macro 'NP'.
      
   3. Do additional simulations using different values for 'OP', 'NP' and 
      'SEED' and see what happens. For example:
      
      $ iverilog -DOP=1 -DNP=40 -DSEED=2 alu.v alu_tb.v
      $ vpp a.out
      
   4. Design an arithmetic unit similar to the example with the following 
      operations (there are no logic operations):
 
      op[2:0]   Operation       f
      -------------------------------
      000       Addition        a + b
      001       Subtraction     a - b
      010       Increment 1     a + 1
      011       Decrement 1     a - 1
      100       Increment 2     a + 2
      101       Decrement 2     a - 2
      110       Increment 4     a + 4
      111       Decrement 4     a - 4
      
      Include an overflow output 'ov'.
*/
